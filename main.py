#!/usr/bin/env python3
from collections import defaultdict
from io import TextIOWrapper
import typing
from os import makedirs
import os.path as op
import logging

from tqdm.auto import tqdm
import numpy as np
import numpy.typing as npt
import click


from gardensim.element import Element
from gardensim.generation import generate_random_garden
from gardensim.garden import Garden
from gardensim.utils.videoutils import make_video
from gardensim.utils.plotutils import add_text

logging.basicConfig(level=logging.INFO, format="%(levelname)s:%(message)s")


def step_closure(
    garden: Garden,
    step: int,
    output_dir: str,
    log_file: TextIOWrapper | None = None,
    class_to_n_element_per_step: (
        typing.Dict[typing.Type, npt.NDArray[np.int_]] | None
    ) = None,
) -> str:
    if log_file is not None:
        log_file.write(f"Step {step}\n")
        log_file.write(garden.get_summary())

    fig = garden.plot_pil()
    add_text(text=f"Step {step}", image=fig)

    image_path = op.join(output_dir, f"garden_{step}.png")
    fig.save(image_path)
    fig.close()

    if class_to_n_element_per_step is not None:
        class_to_n_element = garden.get_class_to_n_elements()
        for class_, n_element in class_to_n_element.items():
            class_to_n_element_per_step[class_][step] = n_element

    return image_path


def plot_class_to_n_element_per_step(
    class_to_n_element_per_step: typing.Dict[
        typing.Type[Element], npt.NDArray[np.int_]
    ],
    output_path: str,
) -> None:
    """Plot the number of elements of each class at each step."""

    import matplotlib.pyplot as plt
    from matplotlib.figure import Figure
    from matplotlib.axes import Axes

    fig: Figure
    ax: Axes
    fig, ax = plt.subplots()
    for class_, n_elements_per_step in class_to_n_element_per_step.items():
        ax.plot(n_elements_per_step, label=class_.__name__, color=class_.get_color())

    ax.set_xlabel("Step")
    ax.set_ylabel("Number of elements")
    ax.grid(color="grey")
    ax.legend()
    fig.savefig(output_path)
    plt.close(fig)
    logging.info(f"Proportions saved in {output_path}")


@click.command()
@click.option("-n", "--n-steps", type=int, default=100, help="Number of steps.")
@click.option(
    "-s",
    "--garden-size",
    nargs=2,
    type=click.Tuple([int, int]),
    default=(100, 100),
    help="Size of the garden (rows, columns).",
)
@click.option("-l", "--n-lettuces", type=int, default=None, help="Number of lettuces.")
@click.option("-w", "--n-worms", type=int, default=None, help="Number of worms.")
@click.option("-o", "--output-dir", type=str, default="output", help="Output directory")
@click.option("--log", is_flag=True, type=bool, default=False, help="Whether to log.")
@click.option(
    "-p",
    "--plot",
    is_flag=True,
    type=bool,
    default=False,
    help="Whether to plot proportions.",
)
def simulate_garden(
    n_steps: int = 10,
    garden_size: typing.Tuple[int, int] = (100, 100),
    n_lettuces: int | None = None,
    n_worms: int | None = None,
    output_dir: str | None = None,
    log: bool = False,
    plot: bool = False,
) -> None:
    """Simulate a garden of size `garden_size` for `n_steps` steps.

    Save the garden into a PNG image at each step and create a video from the images
    at the end.

    Notes:
        Use PIL to plot the garden at each step, as matplotlib is a bottleneck.
    """

    if output_dir is None:
        output_dir = "output"
    makedirs(output_dir, exist_ok=True)

    log_file = open(op.join("output", "log.txt"), mode="w") if log else None

    class_to_n_element_per_step = (
        defaultdict(lambda: np.zeros(n_steps + 1, dtype=int)) if plot else None
    )
    assert class_to_n_element_per_step is not None

    garden = generate_random_garden(
        size=garden_size, n_lettuces=n_lettuces, n_worms=n_worms
    )
    image_paths = [
        step_closure(
            garden=garden,
            step=0,
            output_dir=output_dir,
            log_file=log_file,
            class_to_n_element_per_step=class_to_n_element_per_step,
        )
    ]

    try:
        for step in tqdm(range(1, n_steps + 1), desc="Steps"):
            garden.one_step()
            image_paths.append(
                step_closure(
                    garden=garden,
                    step=step,
                    output_dir=output_dir,
                    log_file=log_file,
                    class_to_n_element_per_step=class_to_n_element_per_step,
                )
            )
    except KeyboardInterrupt:
        pass

    logging.info("At the end, garden has " + garden.get_short_summary())

    if log_file is not None:
        log_file.close()
        logging.info(f"Log saved in {log_file.name}")

    if class_to_n_element_per_step is not None:
        plot_class_to_n_element_per_step(
            class_to_n_element_per_step,
            output_path=op.join(output_dir, "proportions.pdf"),
        )

    make_video(image_paths, op.join(output_dir, "garden.mp4"))


if __name__ == "__main__":
    simulate_garden()
