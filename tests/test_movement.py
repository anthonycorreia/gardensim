from pytest_mock import MockerFixture
from gardensim.movement import Movement, Direction


def test_direction_get_random(mocker: MockerFixture):
    mocker.patch("gardensim.movement.choice", return_value=Direction.DOWN)
    direction = Direction.get_random()
    assert direction == Direction.DOWN


def test_direction_get_random_from_previous(mocker: MockerFixture):
    mocker.patch("gardensim.movement.random", return_value=0.4)  # < 0.6
    mocker.patch("gardensim.movement.choice", return_value=Direction.DOWN)
    direction = Direction.get_random_from_previous(
        previous=Direction.LEFT, prob_same=0.6
    )
    assert direction == Direction.LEFT

    mocker.patch("gardensim.movement.random", return_value=0.7)  # > 0.5
    mocker.patch("gardensim.movement.choice", return_value=Direction.DOWN)
    direction = Direction.get_random_from_previous(
        previous=Direction.LEFT, prob_same=0.6
    )
    assert direction == Direction.DOWN


def test_movement_init():
    movement = Movement((1, 0))
    assert movement.vector == (1, 0)


def test_from_direction():
    movement = Movement.from_direction(Direction.DOWN, 2)
    assert movement.vector == (-2, 0)


def test_from_random(mocker: MockerFixture):
    mocker.patch(
        "gardensim.movement.Direction.get_random", return_value=Direction.RIGHT
    )
    movement = Movement.from_random()
    assert movement.vector == (0, 1)


def test_from_previous_random(mocker: MockerFixture):
    mocker.patch(
        "gardensim.movement.Direction.get_random_from_previous",
        return_value=Direction.RIGHT,
    )
    movement = Movement.from_previous_random(
        previous=Movement.from_direction(Direction.LEFT), prob_same=0.5, magnitude=4
    )
    assert movement.vector == (0, 4)  # right
