from pytest_mock import MockerFixture

from gardensim.worm import Worm
from gardensim.lettuce import Lettuce
from gardensim.movement import Movement
from gardensim.garden import Garden

def test_worm_init():
    worm = Worm((10, 20))
    assert worm.position == (10, 20)


def test_worm_can_reproduce():
    worm = Worm((10, 20))
    assert not worm.can_reproduce
    worm.age = 10
    assert worm.can_reproduce


def test_worm_random_move(mocker: MockerFixture):
    mocker.patch("gardensim.worm.Movement.from_random", return_value=Movement((1, 0)))
    worm = Worm((10, 20))
    worm.random_move()
    assert worm.position == (11, 20)

    mocker.patch("gardensim.worm.Movement.from_random", return_value=Movement((0, -1)))
    worm._previous_movement = None
    worm.random_move()
    assert worm.position == (11, 19)


def test_live_one_step(mocker: MockerFixture):
    mocker.patch("gardensim.worm.Movement.from_random", return_value=Movement((1, 0)))
    worm = Worm((10, 20), energy=100)
    worm.ENERGY_PER_STEP = 2
    worm.live_one_step(garden=None) # type: ignore
    assert worm._previous_movement == Movement((1, 0))
    assert worm.position == (11, 20)
    assert worm.age == 1
    assert worm.energy == 98


def test_interact_lettuce():
    garden = Garden(size=(100, 100))
    worm = Worm(position=(10, 20), age=10, energy=95)
    lettuce = Lettuce(position=(10, 20), size=20)
    worm.interact(lettuce, garden)
    assert worm.energy == 100
    assert lettuce.size == 15

    worm = Worm(position=(10, 20), age=10, energy=50)
    worm.MAX_EAT = 20
    lettuce = Lettuce(position=(10, 20), size=10)
    worm.interact(lettuce, garden)
    assert worm.energy == 60
    assert lettuce.size == 0


def test_interact_worm():
    garden = Garden(size=(100, 100))
    worm_1 = Worm(position=(10, 20), age=10)
    worm_2 = Worm(position=(10, 20), age=20)
    worm_1.MIN_AGE_TO_REPRODUCE = 10
    worm_2.MIN_AGE_TO_REPRODUCE = 10

    new_elements = worm_1.interact(worm_2, garden)
    assert new_elements is not None
    assert len(new_elements) == 1
    worm = new_elements[0]
    assert isinstance(worm, Worm)
    assert worm.age == 0


def test_interact_worm_no_reproduce():
    garden = Garden(size=(100, 100))
    worm_1 = Worm(position=(10, 20), age=9)
    worm_2 = Worm(position=(10, 20), age=20)
    worm_1.MIN_AGE_TO_REPRODUCE = 10
    worm_2.MIN_AGE_TO_REPRODUCE = 10

    interaction_result = worm_1.interact(worm_2, garden)
    assert interaction_result is None
