from unittest.mock import patch

import numpy as np

from gardensim.garden import Garden
from gardensim.element import Element
from gardensim.lettuce import Lettuce
from gardensim.worm import Worm
from gardensim.movement import Movement, Direction


def test_get_position_to_elements():
    garden = Garden(size=(10, 20))
    with patch.multiple(Element, __abstractmethods__=set()):
        element1 = Element(position=(1, 2))  # type: ignore
        element2 = Element(position=(3, 4))  # type: ignore
        element3 = Element(position=(1, 2))  # type: ignore
        garden.elements = [element1, element2, element3]

        assert garden.get_position_to_elements() == {
            (1, 2): [element1, element3],
            (3, 4): [element2],
        }


def test_garden_wrap_element_position():
    garden = Garden(size=(10, 20))

    with patch.multiple(Element, __abstractmethods__=set()):
        element = Element(position=(-4, 10))  # type: ignore
        garden.wrap_element_position(element)
        assert element.position == (6, 10)

        element = Element(position=(6, 22))  # type: ignore
        garden.wrap_element_position(element)
        assert element.position == (6, 2)


def test_clean_up():
    garden = Garden(size=(10, 20))
    with patch.multiple(Element, __abstractmethods__=set(), is_alive=True):
        element1 = Element(position=(1, 2))  # type: ignore
        element2 = Element(position=(3, 4))  # type: ignore
        element3 = Element(position=(1, 2))  # type: ignore
        garden.elements = [element1, element2, element3]
        # Enforce that one element is dead
        element2.is_alive = False  # type: ignore

        garden.clean_up()
        assert garden.elements == [element1, element3]


def test_to_rgb_grid():
    garden = Garden(size=(2, 2))
    with (
        patch.multiple(Element, __abstractmethods__=set()),
        patch.object(
            Element,
            "get_color",
            side_effect=[
                (1.0, 0.0, 0.0),
                (0.0, 0.0, 1.0),
                (0.0, 1.0, 0.0),
            ],
        ),
    ):
        element1 = Element(position=(0, 0))  # type: ignore
        element2 = Element(position=(0, 0))  # type: ignore
        element3 = Element(position=(1, 1))  # type: ignore
        garden.elements = [element1, element2, element3]

        rgb_grid = garden.to_rgb_grid()
        expected_rgb_grid = np.array(
            [
                [[0.5, 0.0, 0.5], [1.0, 1.0, 1.0]],
                [[1.0, 1.0, 1.0], [0.0, 1.0, 0.0]],
            ]
        )
        assert np.equal(rgb_grid, expected_rgb_grid).all()


def test_interaction_element_with_other_worm_lettuce():
    garden = Garden(size=(2, 2))
    lettuce1 = Lettuce(position=(0, 0), size=20)
    lettuce2 = Lettuce(position=(1, 1), size=20)
    worm1 = Worm(position=(0, 0), age=10, energy=95)

    garden.elements = [lettuce1, worm1, lettuce2]
    garden._interaction_element_with_other(worm1, lettuce1)
    assert worm1.energy == 100
    assert lettuce1.size == 15


def test_one_step_interaction():
    garden = Garden(size=(2, 2))
    lettuce1 = Lettuce(position=(0, 0), size=20)
    lettuce2 = Lettuce(position=(1, 1), size=20)
    worm1 = Worm(position=(0, 0), age=10, energy=95)
    worm1.MAX_EAT = 10

    garden.elements = [lettuce1, worm1, lettuce2]
    garden._one_step_interaction()

    assert worm1.energy == 100
    assert lettuce1.size == 15
    assert worm1._next_movement == Movement.from_direction(direction=Direction.STILL)


def test_get_class_to_elements():
    garden = Garden(size=(2, 2))
    lettuce1 = Lettuce(position=(0, 0))
    lettuce2 = Lettuce(position=(1, 1))
    worm1 = Worm(position=(0, 0))
    garden.elements = [lettuce1, worm1, lettuce2]

    class_to_elements = garden.get_class_to_elements()
    expected_class_to_elements = {Lettuce: [lettuce1, lettuce2], Worm: [worm1]}
    assert class_to_elements == expected_class_to_elements
