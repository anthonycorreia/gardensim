from gardensim.lettuce import Lettuce


def test_is_alive():
    lettuce = Lettuce((10, 20))
    assert lettuce.is_alive
    lettuce.size = 0
    assert not lettuce.is_alive


def test_live_one_step():
    lettuce = Lettuce((10, 20), size=20)
    lettuce.SIZE_PER_STEP = 2
    lettuce.live_one_step(garden=None)
    assert lettuce.size == 22
