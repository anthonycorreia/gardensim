import typing
from random import randint

from gardensim.interaction import InteractionResult
from gardensim.movement import Direction
from .element import Element, Position

if typing.TYPE_CHECKING:  # avoid circular imports
    from .garden import Garden


class Lettuce(Element):
    MAX_SIZE = 100
    """Maximum size of the lettuce."""

    SIZE_PER_STEP = 1
    """Size gained by the lettuce in one step."""

    def __init__(self, position: Position, size: int | None = None) -> None:
        super().__init__(position)
        if size is None:
            size = randint(1, self.MAX_SIZE)
        self._size = size

    @property
    def size(self) -> int:
        """Size of the lettuce. The lettuce dies when its size reaches 0."""
        return self._size

    @size.setter
    def size(self, value: int) -> None:
        value = int(value)
        self._size = min(self.MAX_SIZE, max(0, value))

    def _live_one_step(self, garden) -> None:
        if self.size < self.MAX_SIZE:
            self.size += self.SIZE_PER_STEP
        else:
            self.reproduce(garden=garden)

    def reproduce(self, garden: "Garden"):
        random_adjacent_position = garden.get_random_adjacent_position(
            position=self.position, Elements=[self.__class__]
        )
        if random_adjacent_position is not None:
            self.size //= 2
            garden.add_element(
                Lettuce(position=random_adjacent_position, size=1)
            )

    @property
    def is_alive(self) -> bool:
        return self.size > 0

    def _interact(self, other, garden):
        return None

    def interact(self, other, garden):
        return None

    @classmethod
    def get_color(cls):
        return "green"

    @property
    def alpha(self):
        return min(1.0, 5 * self.size / self.MAX_SIZE)

    def get_summary(self) -> str:
        return f"Position {self.position}, " f"size {self.size} / {self.MAX_SIZE}"
