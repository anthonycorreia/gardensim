import typing

import numpy as np
import numpy.typing as npt


def blend_color(
    color: typing.Tuple[float, float, float],
    alpha: float,
    base_color: typing.Tuple[float, float, float] = (1, 1, 1),
) -> typing.Tuple[float, float, float]:
    """Blend a color with another color using a given alpha."""
    blended_color = tuple(
        [
            alpha * color_channel + (1 - alpha) * base_color_channel
            for color_channel, base_color_channel in zip(color, base_color)
        ]
    )
    assert len(blended_color) == 3
    return blended_color


def mix_colors(
    colors: npt.ArrayLike, alphas: npt.ArrayLike | None = None
) -> typing.Tuple[float, float, float]:
    """Get the mixed color of a list of colors.

    Args:
        colors: List of colors to mix.
        alphas: List of alphas for each color.

    Returns:
        The mixed color.
    """
    colors = np.asarray(colors)
    if alphas is None:
        alphas = np.ones(len(colors))

    mixed_color = tuple(np.average(colors, axis=0, weights=alphas))
    assert len(mixed_color) == 3
    return mixed_color


def mix_alphas(alphas: typing.List[float]) -> float:
    return min(sum(alphas), 1.0)
