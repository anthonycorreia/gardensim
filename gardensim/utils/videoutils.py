import typing
import logging

import cv2


def make_video(input_paths: typing.List[str], output_path: str):
    """Create a video from a list of image paths.

    Args:
        image_paths: List of paths to images
    """

    if not input_paths:
        raise ValueError("No input paths provided")

    images = [cv2.imread(input_path) for input_path in input_paths]
    height, width, _ = images[0].shape
    size = (width, height)

    out = cv2.VideoWriter(
        output_path, cv2.VideoWriter_fourcc(*"mp4v"), fps=10, frameSize=size
    )
    for image in images:
        out.write(image)
    out.release()
    logging.info(f"Video saved in {output_path}")
