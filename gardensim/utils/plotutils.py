"""A module containing utilities for plotting."""

import typing
from PIL import ImageDraw, ImageFont
from PIL.Image import Image


def add_text(
    text: str,
    image: Image,
    position: typing.Tuple[int, int] = (0, 0),
    color: typing.Tuple[int, int, int] = (0, 0, 0),
    font: ImageFont.ImageFont | None = None,
    font_size: int = 50,
    **kwargs
):
    """Add text to a PIL image.

    Args:
        text: text to add
        image: image to add the text to
        position: text position
        color: text color
        font: text font
        font_size: text font size. Only used if ``font`` is not set.
        kwargs: Other keyword arguments passed to :py:func:`PIL.ImageDraw.ImageDraw.Draw`
    """
    font_ = ImageFont.load_default(size=font_size) if font is None else font

    draw = ImageDraw.Draw(image)
    draw.text(xy=position, text=text, fill=color, font=font_, **kwargs)
