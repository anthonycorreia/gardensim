from abc import abstractmethod, ABC
import typing
from matplotlib.colors import to_rgb

if typing.TYPE_CHECKING:  # avoid circular imports
    from .interaction import InteractionResult
    from .garden import Garden

Position = typing.Tuple[int, int]


class Element(ABC):
    """An element in a garden."""

    def __init__(self, position: Position) -> None:
        self.position = position
        """Position of the element in the garden."""

        self._can_interact: bool = True
        """Whether the element can interact with other elements currently."""

    @property
    def can_interact(self) -> bool:
        """Whether the element can interact with other elements currently."""
        return self._can_interact and self.is_alive

    @abstractmethod
    def _live_one_step(self, garden: "Garden") -> None:
        """Simulate one step in the garden for this element, without interactions."""
        ...

    def live_one_step(self, garden: "Garden") -> None:
        """Simulate one step in the garden for this element."""
        self._live_one_step(garden=garden)
        self._can_interact = True

    @abstractmethod
    def _interact(
        self, other: "Element", garden: "Garden"
    ) -> "typing.List[Element] | None":
        """Simulate one step in the garden for this element, with interactions.

        Args:
            other: The other element to interact with.

        Returns:
            Interaction result, or None if no interaction happened.
        """
        ...

    def interact(
        self, other: "Element", garden: "Garden"
    ) -> "typing.List[Element] | None":
        """Interact with another element.

        Args:
            other: The other element to interact with.

        Returns:
            Interaction result, or None if no interaction happened.
        """
        if self.can_interact and other.can_interact:
            return self._interact(other, garden)
        else:
            return None

    @property
    @abstractmethod
    def is_alive(self) -> bool:
        """Check if the element is alive."""
        ...

    @classmethod
    @abstractmethod
    def get_color(cls) -> typing.Tuple[float, float, float] | str:
        """Color of the element."""
        ...

    @classmethod
    def get_rgb_color(cls) -> typing.Tuple[float, float, float]:
        """Color of the element as RGB."""
        return to_rgb(cls.get_color())

    @property
    def alpha(self) -> float:
        """Transparency of the element."""
        return 1.0

    @abstractmethod
    def get_summary(self) -> str:
        """Get a summary of the state of the element."""
        ...
