import typing
from collections import defaultdict
from random import choice, shuffle


from PIL import Image
import numpy as np
import numpy.typing as npt
from matplotlib.colors import to_rgb
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes

from gardensim.interaction import InteractionResult
from gardensim.movement import Direction

from .utils.colorutils import mix_colors, mix_alphas, blend_color
from .element import Element, Position

T = typing.TypeVar("T")


class Garden:
    """A garden, corresponding to a multi-dimensional grid containing elements."""

    def __init__(self, size: typing.Tuple[int, int]) -> None:
        self.size = size
        """Size of the garden (rows, columns)."""

        self.elements: typing.List[Element] = []
        """List of all elements in the garden."""

    @property
    def n_cells(self) -> int:
        """Number of cells in the garden."""
        return self.size[0] * self.size[1]

    def get_possible_adjacent_positions(
        self,
        position: Position,
        Elements: typing.List[typing.Type[Element]] | None = None,
        
    ) -> typing.List[Position]:
        """Get the positions of the adjacent cells to a position."""
        position_to_lettuces = self.get_position_to_elements(
            Elements=Elements
        )

        return [
            adjacent_position
            for direction in Direction.get_possible_directions(allow_still=False)
            if (
                adjacent_position := self.wrap_position(
                    (
                        position[0] + direction.value[0],
                        position[1] + direction.value[1],
                    )
                )
            )
            not in position_to_lettuces
        ]
    
    def get_random_adjacent_position(
        self,
        position: Position,
        Elements: typing.List[typing.Type[Element]] | None = None,
    ) -> Position | None:
        """Get a random adjacent position to a position."""
        possible_positions = self.get_possible_adjacent_positions(
            position=position, Elements=Elements
        )
        if possible_positions:
            return choice(possible_positions)
        else:
            return None

    def add_element(self, element: Element) -> None:
        """Add an element to the garden."""
        self.elements.append(element)

    def get_position_to_elements(
        self, Elements: typing.List[typing.Type[Element]] | None = None
    ) -> typing.Dict[Position, typing.List[Element]]:
        """Return a dictionary mapping positions to elements."""
        pos_to_elements: typing.Dict[Position, typing.List[Element]] = defaultdict(list)
        for element in self.elements:
            if Elements is None or type(element) in Elements:
                pos_to_elements[element.position].append(element)
        return pos_to_elements

    def get_class_to_elements(
        self,
    ) -> typing.Dict[typing.Type[Element], typing.List[Element]]:
        class_to_elements = defaultdict(list)
        for element in self.elements:
            class_to_elements[type(element)].append(element)
        return class_to_elements

    def get_class_to_n_elements(
        self,
    ) -> typing.Dict[typing.Type[Element], int]:
        """Get the number of elements of each class in the garden."""
        class_to_elements = self.get_class_to_elements()
        return {class_: len(elements) for class_, elements in class_to_elements.items()}

    def get_summary(self) -> str:
        """Get a summary of the garden state."""
        class_to_elements = self.get_class_to_elements()
        summary = ""
        for class_, elements in class_to_elements.items():
            summary += f"{class_.__name__}:\n"
            for idx, element in enumerate(elements):
                summary += f"{idx}: {element.get_summary()} \n"
        return summary

    def get_short_summary(self) -> str:
        """Get a short summary of the garden state."""
        class_to_elements = self.get_class_to_elements()
        return ", ".join(
            [
                f"{len(elements)} {class_.__name__.lower()}"
                for class_, elements in class_to_elements.items()
            ]
        )

    def wrap_position(self, pos: Position) -> Position:
        """Wrap a position around the garden."""
        row, col = pos
        n_rows, n_cols = self.size
        return (row % n_rows, col % n_cols)

    def wrap_element_position(self, element: Element) -> None:
        """Wrap the position of an element around the garden."""
        element.position = self.wrap_position(element.position)

    def _one_step_no_interaction(self) -> None:
        """Simulate one step in the garden without interactions."""
        for element in self.elements:
            element.live_one_step(garden=self)
            self.wrap_element_position(element)

    def _one_step_interaction(self) -> None:
        """Simulate one step in the garden with interactions.

        One element can interact with only one other element at the same position.
        """
        position_to_elements = self.get_position_to_elements()
        for elements_in_same_position in position_to_elements.values():
            # Interact with a random element
            if len(elements_in_same_position) > 1:
                # List of elements that can interact
                # Will be modified in place during iteration if the element
                # is not longer interactive
                interactive_elements = elements_in_same_position.copy()

                for element in elements_in_same_position:
                    if element.can_interact:
                        # Loop over elements and interact with interactive elements
                        self._interaction_element_with_others(
                            element=element, interactive_elements=interactive_elements
                        )

    def _interaction_element_with_others(
        self, element: Element, interactive_elements: typing.List[Element]
    ) -> None:
        """Interact an element with one other element at random.

        Args:
            element: The element to interact with.
            interactive_elements: List of interactive elements
                If the interaction between ``element`` and an element in
                ``interactive_elements`` makes one of the 2 elements unable to interact,
                it (they) is (are) removed from the list in-place.

        Returns:
            The other element that interacted with ``element``, or None
            if ``element`` did not interact with any other element.
        """
        # Shuffle interactive elements
        left_interactive_elements = interactive_elements.copy()
        shuffle(left_interactive_elements)

        # Randomly loop over other elements until an interaction happens
        while element.can_interact and left_interactive_elements:
            interactive_element = left_interactive_elements.pop()
            if interactive_element is not element:
                self._interaction_element_with_other(element, interactive_element)

    def _interaction_element_with_other(
        self, element: Element, other: Element
    ) -> None:
        """Interact an element with another element."""
        new_elements = element.interact(other, garden=self)
        if new_elements is not None:
            # Wrap positions in case the interaction moved the elements
            self.wrap_element_position(element)
            self.wrap_element_position(other)

            # Add the new elements to the garden
            for new_element in new_elements:
                self.wrap_element_position(new_element)
                self.elements.append(new_element)

    def clean_up(self):
        """Remove dead elements from the garden."""
        self.elements = [element for element in self.elements if element.is_alive]

    def one_step(self) -> None:
        """Simulate one step in the garden."""
        self._one_step_no_interaction()
        self.clean_up()
        self._one_step_interaction()
        self.clean_up()

    @property
    def rgb_base_color(self) -> typing.Tuple[float, float, float]:
        """Color of the garden when there is nothing."""
        return to_rgb("white")

    def to_rgb_grid(self) -> npt.NDArray[np.float64]:
        """Convert the garden to a grid of RGB colors."""
        grid = np.full((*self.size, 3), self.rgb_base_color, dtype=float)

        position_to_elements = self.get_position_to_elements()

        for position, elements in position_to_elements.items():
            colors = [element.get_rgb_color() for element in elements]
            alphas = [element.alpha for element in elements]
            mixed_color = mix_colors(colors, [element.alpha for element in elements])
            mixed_alpha = mix_alphas(alphas)

            final_color = blend_color(
                color=mixed_color, alpha=mixed_alpha, base_color=self.rgb_base_color
            )
            grid[position] = final_color
        return grid

    @typing.overload
    def plot_mpl(
        self, fig: None = ..., ax: None = ...
    ) -> typing.Tuple[Figure, Axes]: ...

    @typing.overload
    def plot_mpl(self, fig: None = ..., ax: Axes = ...) -> typing.Tuple[None, Axes]: ...

    @typing.overload
    def plot_mpl(
        self, fig: Figure = ..., ax: Axes = ...
    ) -> typing.Tuple[Figure, Axes]: ...

    def plot_mpl(
        self, fig: Figure | None = None, ax: Axes | None = None
    ) -> typing.Tuple[Figure | None, Axes]:
        """Create a figure and axes with the garden plot."""
        rgb_grid = self.to_rgb_grid()

        if ax is None:
            if fig is not None:
                raise ValueError("If `fig` is provided, `ax` must be provided too.")
            fig, ax = plt.subplots()
            assert ax is not None

        ax.imshow(rgb_grid)

        ax.grid(which="both", color="black", linestyle="-", linewidth=0.5)
        ax.set_yticks(np.arange(-0.5, rgb_grid.shape[0], 1))
        ax.set_xticks(np.arange(-0.5, rgb_grid.shape[1], 1))
        ax.tick_params(labelbottom=False, labelleft=False, length=0)
        if fig is not None:
            fig.tight_layout()

        return fig, ax

    def plot_pil(self) -> Image.Image:
        """Plot the garden using PIL.

        Returns:
            The image of the garden.
        """
        rgb_grid = self.to_rgb_grid()
        image = Image.fromarray((rgb_grid * 255).astype(np.uint8))

        # Resize the image if it is too small
        min_size = 1000
        current_max_size = max(image.size)
        if current_max_size < min_size:
            resize_factor = min_size / current_max_size
            new_size = (
                int(image.size[0] * resize_factor),
                int(image.size[1] * resize_factor),
            )
            image = image.resize(new_size, Image.Resampling.NEAREST)
        return image
