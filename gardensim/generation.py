"""A module to generate a random initial garden."""

import typing
from random import randrange
import logging

import numpy as np

from .garden import Garden
from .lettuce import Lettuce
from .worm import Worm


def get_random_coordinates(
    size: typing.Tuple[int, int], n: int
) -> typing.List[typing.Tuple[int, int]]:
    """Get `n` distinct random coordinates in a grid of size `size`.

    Args:
        size: Size of the grid (rows, columns).
        n: Number of coordinates to generate.

    Returns:
        List of `n` random coordinates.
    """

    total_size = size[0] * size[1]
    if n > total_size:
        raise ValueError(
            "Number of elements is greater than the total number of cells."
        )

    random_integers = np.random.choice(total_size, n, replace=False)
    random_xs = random_integers // size[1]
    random_ys = random_integers % size[1]
    return list(zip(random_xs, random_ys))


def generate_random_garden(
    size: typing.Tuple[int, int] = (10, 10),
    n_lettuces: int | None = None,
    n_worms: int | None = None,
) -> Garden:
    garden = Garden(size=size)
    logging.info(f"Garden of size {size} created.")

    # Default values
    n_cells = garden.n_cells
    if n_lettuces is None:
        n_lettuces = max(1, randrange(n_cells) // 4)
    if n_worms is None:
        n_worms = max(1, randrange(n_cells // 64))

    logging.info(
        f"Populating the garden with {n_lettuces} lettuces and {n_worms} worms."
    )
    random_coordinates = get_random_coordinates(size, n_lettuces + n_worms)

    for random_coordinate in random_coordinates[:n_lettuces]:
        garden.add_element(Lettuce(position=random_coordinate))

    for random_coordinate in random_coordinates[n_lettuces:]:
        garden.add_element(Worm(position=random_coordinate))

    return garden
