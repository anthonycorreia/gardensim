import typing
from enum import Enum
from random import choice, random

from .element import Position


class Direction(Enum):
    """Enumeration for the direction of movement."""

    UP = (1, 0)
    DOWN = (-1, 0)
    LEFT = (0, -1)
    RIGHT = (0, 1)
    STILL = (0, 0)  # special: no movement

    @classmethod
    def get_possible_directions(cls, allow_still: bool = False):
        return [
            direction
            for direction in Direction
            if allow_still or direction != Direction.STILL
        ]

    @classmethod
    def from_tuple(cls, tuple_: tuple[int, int]) -> "Direction":
        return cls(tuple_)

    @classmethod
    def get_random(cls, allow_still: bool = False) -> "Direction":
        possible_directions = cls.get_possible_directions(allow_still=allow_still)
        return choice(possible_directions)

    @classmethod
    def get_random_from_previous(
        cls, previous: "Direction", prob_same: float, allow_still: bool = False
    ) -> "Direction":
        """Return a random direction from the given direction with a given probability."""
        if (
            not (previous == Direction.STILL and not allow_still)
            and random() < prob_same
        ):
            return Direction(previous)
        else:
            return choice(
                [
                    possible_direction
                    for possible_direction in cls.get_possible_directions(
                        allow_still=allow_still
                    )
                    if possible_direction != previous
                ]
            )


class Movement:
    """Class representing a movement in a 2D grid."""

    def __init__(self, vector: typing.Tuple[int, int]):
        self.vector = vector
        """Vector of the movement."""

    @classmethod
    def from_direction(cls, direction: Direction, magnitude: int = 1) -> "Movement":
        """Return a movement from a given direction and magnitude.

        Args:
            direction: Direction of movement.
            magnitude: Magnitude of movement. Default is 1.
        """
        vector = tuple(coord * magnitude for coord in direction.value)
        assert len(vector) == 2
        return cls(vector)

    @classmethod
    def from_random(cls, magnitude: int = 1, allow_still: bool = False) -> "Movement":
        """Return a random movement.

        Args:
            magnitude: Magnitude of movement. Default is 1.
            allow_still: Whether to allow still movement. Default is False.
        """
        return cls.from_direction(
            direction=Direction.get_random(allow_still=allow_still), magnitude=magnitude
        )

    @classmethod
    def from_previous_random(
        cls,
        previous: "Movement",
        prob_same: float,
        magnitude: int = 1,
        allow_still: bool = False,
    ) -> "Movement":
        """Return a random movement from the given direction with a given probability
        to be on the same direction.

        Args:
            previous: Previous movement.
            prob_same: Probability to be on the same direction.
        """
        return cls.from_direction(
            direction=Direction.get_random_from_previous(
                previous=Direction(previous.vector),
                prob_same=prob_same,
                allow_still=allow_still,
            ),
            magnitude=magnitude,
        )

    @property
    def no_movement(self) -> bool:
        return self.vector == (0, 0)

    def get_new_position(self, position: Position) -> Position:
        """Return the new position after moving in the given direction.

        Args:
            position: Current position.

        Returns:
            New position after moving in the given direction.
        """

        if self.no_movement:
            return position
        else:
            new_position = tuple(
                [
                    position_coord + direction_coord
                    for position_coord, direction_coord in zip(position, self.vector)
                ]
            )
            assert len(new_position) == 2
            return new_position

    def __eq__(self, __value: object) -> bool:
        if not isinstance(__value, Movement):
            return False

        return self.vector == __value.vector
