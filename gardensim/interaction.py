import typing
from dataclasses import dataclass, field

from .element import Element


@dataclass
class InteractionResult:
    """Result of an interaction between two elements."""

    new_elements: typing.List[Element] = field(default_factory=list)
    """New elements resulting from the interaction."""
