from __future__ import annotations
import typing
import logging


from .element import Element, Position
from .lettuce import Lettuce
from .movement import Direction, Movement


class Worm(Element):
    MAX_ENERGY: int = 100
    """Maximum energy of the worm."""

    MAX_EAT: int = 25
    """Maximum amount of lettuce that the worm can eat in one step."""

    MIN_AGE_TO_REPRODUCE: int = 10
    """Minimum age of the worm to reproduce.
    The worm cannot reproduce until it reaches this age."""

    PROB_SAME_DIRECTION: float = 0.8
    """Probability to move in the same direction as the previous movement."""

    ENERGY_PER_STEP: int = 3
    """Energy consumed by the worm in one step."""

    ENERGY_PER_STEP_STILL: int = 3
    """Energy consumed by the worm in one step when it does not move.
    
    Needs to be higher than :py:data:`Lettuce.SIZE_PER_STEP`, otherwise the worm
    would eat the lettuce without moving once it met one.
    """

    def __init__(
        self, position: Position, age: int = 0, energy: int = MAX_ENERGY
    ) -> None:
        super().__init__(position)

        self._energy = energy

        self._age = age

        self._previous_movement: Movement | None = None
        """Previous movement of the worm. Used to avoid brownian motion.
        
        If ``None``, the previous movement is not existant.
        """

        self._next_movement: Movement | None = None
        """Next movement of the worm. Used to enforce the worm to move in a certain
        direction, typically after an interaction.
        """

    @property
    def energy(self) -> int:
        """Energy of the worm. The worm dies when its energy reaches 0.
        The worm gains energy by eating lettuce and loses energy by moving."""
        return self._energy

    @energy.setter
    def energy(self, value: int) -> None:
        value = int(value)
        self._energy = max(0, min(value, self.MAX_ENERGY))

    @property
    def age(self) -> int:
        """Age of the worm.
        The worm cannot reproduce until its age reaches 10.
        """
        return self._age

    @age.setter
    def age(self, value: int) -> None:
        value = int(value)
        if not 0 <= value:
            raise ValueError(f"Age must be greater than 0 but is {value}.")
        self._age = value

    @property
    def can_reproduce(self) -> bool:
        """Return whether the worm can reproduce given its age."""
        return self.age >= self.MIN_AGE_TO_REPRODUCE

    def random_move(self) -> Movement:
        """Move the worm in a random direction.

        Returns:
            Random movement of the worm
        """
        if self._next_movement is not None:
            next_movement = self._next_movement
            self._next_movement = None
        elif (previous_movement := self._previous_movement) is not None:
            next_movement = Movement.from_previous_random(
                previous_movement, self.PROB_SAME_DIRECTION
            )
            assert not next_movement.no_movement
        else:
            next_movement = Movement.from_random()
            assert not next_movement.no_movement

        # Update worm state
        self._previous_movement = next_movement
        self.position = next_movement.get_new_position(self.position)
        return next_movement

    def _live_one_step(self, garden) -> None:
        # Move
        movement = self.random_move()
        # Update age and energy
        self.age += 1
        self.energy -= (
            self.ENERGY_PER_STEP_STILL if movement.no_movement else self.ENERGY_PER_STEP
        )

    @property
    def is_alive(self) -> bool:
        return self.energy > 0
    
    def _interact(self, other, garden) -> typing.List[Element] | None:
        assert self.position == other.position
        if isinstance(other, Lettuce):  # Worm eat lettuce
            # How much lettuce can the worm eat
            eaten = min(self.MAX_EAT, other.size, self.MAX_ENERGY - self.energy)
            assert eaten > 0, (
                f"min({self.MAX_EAT}, {other.size}, {self.MAX_ENERGY - self.energy}) "
                f"= {eaten}"
            )
            # Eat the lettuce
            self.energy += eaten
            other.size -= eaten
            assert self.energy <= self.MAX_ENERGY

            # if there is still lettuce left, the worm stays at the same position
            if other.size > 0:
                self._next_movement = Movement.from_direction(direction=Direction.STILL)
            self._can_interact = False
            return []
        elif isinstance(other, Worm):  # WooHoo
            if self.can_reproduce and other.can_reproduce:
                random_adjacent_position = garden.get_random_adjacent_position(
                    position=self.position, Elements=[self.__class__]
                )
                if random_adjacent_position is not None:
                    average_energy = (self.energy + other.energy) // 2
                    self.energy //= 2
                    other.energy //= 2
                    baby_worm = Worm(
                        position=random_adjacent_position, energy=average_energy
                    )
                    # the worms can no longer interact during this step
                    self._can_interact = False
                    other._can_interact = False
                    return [baby_worm]
                else:
                    return None
            else:
                return None
        else:
            logging.debug(
                f"Worm does not know how to interact with {other.__class__.__name__}"
            )

    @classmethod
    def get_color(cls):
        return "r"

    @property
    def alpha(self):
        return min(1.0, 5 * self.energy / self.MAX_ENERGY)

    def get_summary(self) -> str:
        return (
            f"Position {self.position}, "
            f"age {self.age}, "
            f"energy {self.energy} / {self.MAX_ENERGY}"
        )
